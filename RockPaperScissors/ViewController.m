//
//  ViewController.m
//  RockPaperScissors
//
//  Created by Robert Randell on 23/12/2013.
//  Copyright (c) 2013 Robert Randell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)challenge:(id)sender {
    
    unsigned long int tagNumber = [sender tag];
    
    int low_bound = 1;
    int high_bound = 6;
    int rndValue = low_bound + arc4random() % (high_bound - low_bound);
    
    NSString *computerHand = @"";
    
    switch (rndValue) {
        case 1:
            computerHand = @"Rock";
            break;
            
        case 2:
            computerHand = @"Paper";
            break;
            
        case 3:
            computerHand = @"Scissors";
            break;
            
		case 4:
            computerHand = @"Lizard";
            break;
            
        case 5:
            computerHand = @"Spock";
            break;

        default:
            break;
    }
    //SK 20140226 source http://en.wikipedia.org/wiki/Rock-paper-scissors-lizard-Spock
    NSString *rules[10] = {
        @"Scissors cut paper",
        @"Paper covers rock",
        @"Rock crushes lizard",
        @"Lizard poisons Spock",
        @"Spock smashes scissors",
        @"Scissors decapitate lizard",
        @"Lizard eats paper",
        @"Paper disproves Spock",
        @"Spock vaporizes rock",
        @"Rock crushes scissors"
    };

    NSString *wins[3] = {  @"It's a draw.", @" - You win!", @" - Computer wins." };
    
    //int combos[5][5];
    //rows: player, columns: puter, values: 0, 1, 2 correspond to index of wins, second number 0-9 corresponds to index of rules
    int combos[5][5] = {
        {0,21,19,12,28},
        {11,0,20,26,17},
        {29,10,0,15,24},
        {22,16,25,0,13},
        {18,27,14,23,0}
    };
    
    int res = combos[tagNumber][rndValue-1];
    int txt_id = 0;
    int win_id = 0;
    
    NSString *puterText = [NSString string];
    NSString *winText = [NSString string];
    NSString *ruleText = [NSString string];
    NSString *resultText = [NSString string];
    
    puterText = [NSString stringWithFormat:@"Computer picked %@. ",computerHand];

      if (res != 0) {
        win_id = res / 10;
        txt_id = res - (win_id*10);
        
        ruleText = rules[txt_id];
      }
    winText = wins[win_id];

    
    resultText = [NSString stringWithFormat:@"%@%@%@",puterText,ruleText,winText];

    _result.text = resultText;
 
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
