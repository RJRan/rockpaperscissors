//
//  AppDelegate.h
//  RockPaperScissors
//
//  Created by Robert Randell on 23/12/2013.
//  Copyright (c) 2013 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
