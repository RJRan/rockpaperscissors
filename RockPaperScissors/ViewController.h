//
//  ViewController.h
//  RockPaperScissors
//
//  Created by Robert Randell on 23/12/2013.
//  Copyright (c) 2013 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIButton *rockBtn;
@property (nonatomic, retain) IBOutlet UIButton *paperBtn;
@property (nonatomic, retain) IBOutlet UIButton *scissorsBtn;
@property (nonatomic, retain) IBOutlet UIButton *lizardBtn;
@property (nonatomic, retain) IBOutlet UIButton *spockBtn;

@property (nonatomic, retain) IBOutlet UILabel *result;

- (IBAction)challenge:(id)sender;

@end
