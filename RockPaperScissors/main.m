//
//  main.m
//  RockPaperScissors
//
//  Created by Robert Randell on 23/12/2013.
//  Copyright (c) 2013 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
